from  sklearn import tree
'''
    Video tutorial : https://www.youtube.com/watch?v=cKxRvEZd3Mw
    the format of the single feature is [weight, surfaceType]. weight is in gms, surface type is 0 or 1 depending upon
    whether it is bumpy or smooth. Also, in labels, Apple denote 1 whereas orange is denoted by 0.
'''
features = [[140, 1], [130, 1], [150, 0], [170, 0]]
labels = ["apple",  "apple", "orange", "orange"]
clf = tree.DecisionTreeClassifier()
clf.fit(features, labels)
print clf.predict([[160, 0]])